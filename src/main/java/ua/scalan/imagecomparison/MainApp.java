package ua.scalan.imagecomparison;

import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static java.awt.Color.red;


public class MainApp extends Application {


    public static void main(String[] args) {
        launch(args);
    }

    private Desktop desktop = Desktop.getDesktop();
    BufferedImage bufferedImage1 = null;
    BufferedImage bufferedImage2 = null;

    @Override
    public void start(final Stage primaryStage) {
        final StackPane root = new StackPane();
        final FileChooser fileChooser = new FileChooser();
        final Button btn = new Button();
        final Button open1 = new Button();
        final Button open2 = new Button();
        final Text route1 = new Text();
        final Text route2 = new Text();
        final VBox vBox = new VBox();

        open1.setText("Open first file");
        open1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                File file = fileChooser.showOpenDialog(primaryStage);
                if (file != null) {
                    try {
                        bufferedImage1 = ImageIO.read(new File(String.valueOf(file)));
                        route1.setText(file.toString());
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }
        });

        open2.setText("Open second file");
        open2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                File file = fileChooser.showOpenDialog(primaryStage);
                if (file != null) {
                    try {
                        bufferedImage2 = ImageIO.read(new File(String.valueOf(file)));
                        route2.setText(file.toString());
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }
        });

        primaryStage.setTitle("Image Comparison Requirements");

        btn.setText("execute'");
        btn.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                if (bufferedImage1 != null && bufferedImage2 != null){

                    int startX = -1;
                    int startY = -1;
                    int endX = bufferedImage1.getWidth();
                    int endY = bufferedImage1.getHeight();
                    long col1;
                    long col2;
                    long percent;

                    final BufferedImage newBufferedImage = new BufferedImage(bufferedImage1.getWidth(),
                            bufferedImage1.getHeight(), BufferedImage.TYPE_INT_RGB);
                    Graphics g = newBufferedImage.createGraphics();
                    g.setColor(red);

                    for (int x = 0; x < bufferedImage1.getWidth() - 1; x++) {
                        for (int y = 0; y < bufferedImage1.getHeight() - 1; y++) {
                            col1 = bufferedImage1.getRGB(x, y);
                            col2 = bufferedImage2.getRGB(x, y);
                            percent = col1 * 100 / col2;
                            if (percent > 110 || percent < 90) {
                                if (x > endX + 10) {
                                    g.drawRect(startX, startY, endX - startX, endY - startY);
                                    startX = -1;
                                    startY = -1;
                                    endX = bufferedImage1.getWidth();
                                    endY = bufferedImage1.getHeight();
                                }
                                if (startX == -1) {
                                    startX = x;
                                }
                                if (x <= startX) {
                                    startX = x;
                                }
                                if (startY == -1) {
                                    startY = y;
                                }
                                if (y <= startY) {
                                    startY = y;
                                }
                                if (endX == bufferedImage1.getWidth()) {
                                    endX = x;
                                }
                                if (x >= endX) {
                                    endX = x;
                                }
                                if (endY == bufferedImage1.getHeight()) {
                                    endY = y;
                                }
                                if (y >= endY) {
                                    endY = y;
                                }

                                newBufferedImage.setRGB(x, y, bufferedImage2.getRGB(x, y));
                            } else {
                                newBufferedImage.setRGB(x, y, bufferedImage2.getRGB(x, y));
                            }
                        }
                    }
                    g.drawRect(startX, startY, endX - startX, endY - startY);
                    System.out.println("Готово!");

                    Image image = SwingFXUtils.toFXImage(newBufferedImage, null);
                    ImageView iv1 = new ImageView();
                    iv1.setImage(image);
                    iv1.setFitHeight(500);
                    iv1.setFitWidth(800);
                    vBox.getChildren().add(iv1);

                    Button save = new Button();
                    save.setText("Save file");
                    save.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent actionEvent) {
                            FileChooser fileChooser = new FileChooser();
                            fileChooser.setTitle("Save Image");
                            File file = fileChooser.showSaveDialog(primaryStage);
                            if (file != null) {
                                try {
                                    ImageIO.write(newBufferedImage, "jpg", file);
                                } catch (IOException ex) {
                                    System.out.println(ex.getMessage());
                                }
                            }
                        }
                    });
                    vBox.getChildren().add(save);
                }  else {
                    Text error = new Text();
                    error.setText("No file");
                    vBox.getChildren().add(error);
                }
            }
        });

        vBox.getChildren().add(open1);
        vBox.getChildren().add(route1);
        vBox.getChildren().add(open2);
        vBox.getChildren().add(route2);
        vBox.getChildren().add(btn);
        root.getChildren().add(vBox);
        primaryStage.setScene(new Scene(root, 900, 700));
        primaryStage.show();
    }
}